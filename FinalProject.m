clear variables
close all
% Open movie file.
movieObj = VideoReader('Arimaa.wmv');
writerObj = VideoWriter('finalVideo');
writerObj.FrameRate = 15;
open(writerObj);
nFrames = movieObj.NumberOfFrames;
checkerHistogram = zeros(8, 8, 2);
MOMENTS = zeros(2, 7, 3); % Empty, Rabbit, Cat, Dog, Horse, Camel, Elephant 1=WHITE CIRCLE 2=BLACK CIRCLE
fileID = fopen('calibration.calib','r');
formatSpec = '%f %f %f f %f %f f %f %f f %f %f f %f %f f %f %f f %f %f';
activePlayer = 1;
passivePlayer = 2;
for i=1:2
    for j=1:7
        A = fscanf(fileID, formatSpec);
        MOMENTS(i, j, 1) = A(1,1);
        MOMENTS(i, j, 2) = A(2,1);
        MOMENTS(i, j, 3) = A(3,1);
    end
end
fclose(fileID);

for iFrame=1:nFrames
    I = read(movieObj,iFrame);
    checkerHistogram = zeros(8, 8, 2);
    figure(1), imshow(I), title(sprintf('Frame %d', iFrame));
    [corners, nMatches, avgErr] = findCheckerBoard(I);
    figure(1), imshow(I);
    line([corners(1, 1) corners(2, 1)], [corners(1, 2) corners(2, 2)], 'LineWidth', 3)
    line([corners(1, 1) corners(4, 1)], [corners(1, 2) corners(4, 2)], 'LineWidth', 3)
    line([corners(2, 1) corners(3, 1)], [corners(2, 2) corners(3, 2)], 'LineWidth', 3)
    line([corners(3, 1) corners(4, 1)], [corners(3, 2) corners(4, 2)], 'LineWidth', 3)
    fixed = [0 0; 400 0; 400 400; 0 400];
    tform = fitgeotrans(corners, fixed, 'projective');
    I2 = imwarp(I, tform, 'OutputView', imref2d([400, 400], [0, 400], [0, 400]));
    I3 = rgb2gray(I2);
    
    for i=1:50:400
        for j=1:50:400
            I5 = I3(i:i+49, j:j+49);
            I5 = im2bw(I5, 0.5);
            %if (I5(25,25) == 0)
            if (I5(27,43) == 1)
                I5(:,1:11) = 1;
                I5(1:11,:) = 1;
                I5(:,39:50) = 1;
                I5(39:50,:) = 1;
            else
                I5(:,1:11) = 0;
                I5(1:11,:) = 0;
                I5(:,39:50) = 0;
                I5(39:50,:) = 0;
            end
            %invariant = Hu_Moments(I5);
            invariant(1) = moment(I5, 1, 0);
            invariant(2) = moment(I5, 0, 1);
            invariant(3) = moment(I5, 1, 1);
            mindistance = 100000000000000;
            color = 1;
            piece = 1;
            for k=1:2
                for m=1:7
                    if sqrt((MOMENTS(k,m,1)- invariant(1))^2 + (MOMENTS(k,m,2)- invariant(2))^2 + (MOMENTS(k,m,3)- invariant(3))^2) < mindistance
                        mindistance = sqrt((MOMENTS(k,m,1)- invariant(1))^2 + (MOMENTS(k,m,2)- invariant(2))^2 + (MOMENTS(k,m,3)- invariant(3))^2);
                        color = k;
                        piece = m;
                    end
                end
            end
            checkerHistogram((i+49)/50, (j+49)/50, color) = piece-1;
        end
    end
    % checkerHistogram(:, :, 1)
     %checkerHistogram(:, :, 2)
    figure (3), imshow(I2);
    for i=1:8
        for j=1:8
            if i > 1
                if checkerHistogram(i, j, activePlayer) > checkerHistogram(i-1, j, passivePlayer) && checkerHistogram(i-1, j, activePlayer) == 0
                    line([j*50-25, j*50-25], [i*50-25, (i-1)*50-25], 'LineWidth', 3, 'Color', 'blue')
                    if checkerHistogram(i-1, j, passivePlayer) > 0
                        if i > 2
                            if checkerHistogram(i-2, j, :) == 0
                                line([j*50-25, j*50-25], [(i-1)*50-25, (i-2)*50-25], 'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if j > 1
                            if checkerHistogram(i-1, j-1, :) == 0
                                line([j*50-25, (j-1)*50-25],[(i-1)*50-25, (i-1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if j < 8
                            if checkerHistogram(i-1, j+1, :) == 0
                                line([j*50-25, (j+1)*50-25], [(i-1)*50-25, (i-1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                    end
                end
            end
            if i < 8
                if checkerHistogram(i, j, activePlayer) > checkerHistogram(i+1, j, passivePlayer) && checkerHistogram(i+1, j, activePlayer) == 0
                    line([j*50-25, j*50-25], [i*50-25, (i+1)*50-25], 'LineWidth', 3, 'Color', 'blue')
                    if checkerHistogram(i+1, j, passivePlayer) > 0
                        if i < 7
                            if checkerHistogram(i+2, j, :) == 0
                                line([j*50-25, j*50-25], [(i+1)*50-25, (i+2)*50-25], 'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if j > 1
                            if checkerHistogram(i+1, j-1, :) == 0
                                line([j*50-25, (j-1)*50-25],[(i+1)*50-25, (i+1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if j < 8
                            if checkerHistogram(i+1, j+1, :) == 0
                                line([j*50-25, (j+1)*50-25], [(i+1)*50-25, (i+1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                    end
                end
            end
            if j > 1
                if checkerHistogram(i, j, activePlayer) > checkerHistogram(i, j-1, passivePlayer) && checkerHistogram(i, j-1, activePlayer) == 0
                    line([j*50-25, (j-1)*50-25], [i*50-25, i*50-25], 'LineWidth', 3, 'Color', 'blue')
                    if checkerHistogram(i, j-1, passivePlayer) > 0
                        if j > 2
                            if checkerHistogram(i, j-2, :) == 0
                                line([(j-1)*50-25, (j-2)*50-25], [i*50-25, i*50-25], 'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if i > 1
                            if checkerHistogram(i-1, j-1, :) == 0
                                line([(j-1)*50-25, (j-1)*50-25],[i*50-25, (i-1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if i < 8
                            if checkerHistogram(i+1, j-1, :) == 0
                                line([(j-1)*50-25, (j-1)*50-25], [i*50-25, (i+1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                    end
                end
            end
            if j < 8
                if checkerHistogram(i, j, activePlayer) > checkerHistogram(i, j+1, passivePlayer) && checkerHistogram(i, j+1, activePlayer) == 0
                    line([j*50-25, (j+1)*50-25], [i*50-25, i*50-25], 'LineWidth', 3, 'Color', 'blue')
                    if checkerHistogram(i, j+1, passivePlayer) > 0
                        if j < 7
                            if checkerHistogram(i, j+2, :) == 0
                                line([(j+1)*50-25, (j+2)*50-25], [i*50-25, i*50-25], 'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if i > 1
                            if checkerHistogram(i-1, j+1, :) == 0
                                line([(j+1)*50-25, (j+1)*50-25],[i*50-25, (i-1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                        if i < 8
                            if checkerHistogram(i+1, j+1, :) == 0
                                line([(j+1)*50-25, (j+1)*50-25], [i*50-25, (i+1)*50-25],  'LineWidth', 3, 'Color', 'red')
                            end
                        end
                    end
                end
            end
        end
    end
    drawnow
    newFrameOut = getframe;
    writeVideo(writerObj, newFrameOut);
end
close(writerObj);
